
import flask
from flask import Flask,request,Response
from flask_restful import Resource, Api
import pymongo
import csv
from pymongo import MongoClient

app = Flask(__name__)
api = Api(app)

client = MongoClient('db', 27017)
db = client.tododb
collection = db.control


class All(Resource):

	def get(self):
		n = request.args.get('top')
		app.logger.debug('n equals ',n)
		if n == None:
			top = 20
		else:
			top = int(n)
		times = collection.find().sort([("open_time",pymongo.ASCENDING), ("close_time",pymongo.ASCENDING)]).limit(100)
		times_list = []
		for time in times:
			if time['open_time'] != '' and time['close_time'] != '':
				times_list.append((time['open_time'],time['close_time']))
		times_list = times_list[:top]
		app.logger.debug(times_list)
		out = []
		for open_t, close_t in times_list:
			out.append({'open':open_t,'close':close_t})
		return flask.jsonify(result= out)

api.add_resource(All, "/listAll",'/listAll/json')


class Open_only(Resource):
	def get(self):
		n = request.args.get('top')
		if n == None:
			top = 20
		else:
		    top = int(n)
		times = collection.find().sort("open_time",pymongo.ASCENDING).limit(100)
		times_list = []
		for time in times:
			if time['open_time'] != '':
				times_list.append(time['open_time'])
		times_list = times_list[:top]
		times = []
		for open_t in times_list:
		    times.append({'open':open_t})
		return flask.jsonify(result= times)

api.add_resource(Open_only, '/listOpenOnly', "/listOpenOnly/json")

class Close_only(Resource):
	def get(self):
		n = request.args.get('top')
		if n == None:
			top = 20
		else:
			top = int(n)
		times = collection.find().sort("close_time",pymongo.ASCENDING).limit(100)
		times_list = []
		for time in times:
			if time['close_time'] != '':
				times_list.append(time['close_time'])
		times_list = times_list[:top]
		times = []
		for close_t in times_list:
			times.append({'close':close_t})
		return flask.jsonify(result= times)

api.add_resource(Close_only, '/listCloseOnly', "/listCloseOnly/json")

class All_csv(Resource):

	def get(self):
		n = request.args.get('top')
		if n == None:
			top = 20
		else:
			top = int(n)
		times = collection.find().sort([("open_time",pymongo.ASCENDING), ("close_time",pymongo.ASCENDING)]).limit(100)
		times_list = []
		for time in times:
			if time['open_time'] != '' and time['close_time'] != '':
				times_list.append((time['open_time'],time['close_time']))
		times_list = times_list[:top]
		csv = open('data.csv', "w")
		csv.write("Open Time,Close Time\n") 
		for open_t,close_t in times_list:
			row = open_t + "," + close_t + "\n"
			csv.write(row)
		csv = open('data.csv', "r")
		return Response(csv, mimetype='text/csv')
            
api.add_resource(All_csv, "/listAll/csv")

class Open_only_csv(Resource):

	def get(self):
		n = request.args.get('top')
		if n == None:
			top = 20
		else:
			top = int(n)
		times = collection.find().sort("open_time",pymongo.ASCENDING).limit(100)
		times_list = []
		for time in times:
			if time['open_time'] != '':
				times_list.append(time['open_time'])     
		times_list = times_list[:top]
		csv = open('data.csv', "w")
		csv.write("Open Time\n")
		for open_t in times_list:
			row = open_t + "\n"
			csv.write(row)
		csv = open('data.csv', "r")
		return Response(csv, mimetype='text/csv')

api.add_resource(Open_only_csv, "/listOpenOnly/csv")

class Close_only_csv(Resource):

	def get(self):
		n = request.args.get('top')
		if n == None:
			top = 20
		else:
			top = int(n)
		times = collection.find().sort("close_time",pymongo.ASCENDING).limit(100)
		times_list = []
		for time in times:
			if time['close_time'] != '':
				times_list.append(time['close_time']) 
		times_list = times_list[:top]           
		csv = open('data.csv', "w")
		csv.write("Close Time\n")
		for close_t in times_list:
			row = close_t + "\n"
			csv.write(row)
		csv = open('data.csv', "r")
		return Response(csv, mimetype='text/csv')

api.add_resource(Close_only_csv, "/listCloseOnly/csv")     

if __name__ == '__main__':
    app.run(debug=True, port=80, host='0.0.0.0')