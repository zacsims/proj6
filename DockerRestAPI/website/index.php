<html>
    <head>
        <title>Control Database Information</title>
    </head>

    <body>

        <h1>All Times</h1>
        <ul>
            <?php
            $json = file_get_contents('http://laptop-service/listAll');
            $obj = json_decode($json);
            $times = $obj->result;
            if ($times == []){
                echo "Database is empty"; 
            } else{
                foreach($times as $k) {
                    $open = $k->open;
                    $close = $k->close;
                    echo "<li>Open:$open</li><li>Close:$close</li>";
                }
            }
            ?>
        </ul>


        <h1>Open Times Only</h1>
        <ul>
            <?php
            $json = file_get_contents('http://laptop-service/listOpenOnly');
            $obj = json_decode($json);
            $times = $obj->result;
            if ($times == []){
                echo "Database is empty"; 
            } else{
                foreach($times as $k) {
                    $open = $k->open;
                    echo "<li>Open: $open</li>";
                }
            }
            ?>
        </ul>


        <h1>Close Times Only</h1>
        <ul>
            <?php            
            $json = file_get_contents('http://laptop-service/listCloseOnly');
            $obj = json_decode($json);
            $times = $obj->result;
            if ($times == []){
                echo "No Times"; 
            } else{
                foreach($times as $k) {
                    $close = $k->close;
                    echo "<li>Close: $close</li>";
                }
            }
            ?>
        </ul>


        <h1>Top 3 Open & Close Times</h1>
        <ul>
            <?php
            $json = file_get_contents('http://laptop-service/listAll?top=3');
            $obj = json_decode($json);
            $times = $obj->result;
            if ($times == []){
                echo "No Times"; 
            } else{
                foreach($times as $k) {
                    $open = $k->open;
                    $close = $k->close;
                    echo "<li>Open:$open</li><li>Close:$close</li>";
                }
            }
            ?>
        </ul>

        <h1>Top 2 Open Times</h1>
        <ul>
            <?php           
            $json = file_get_contents('http://laptop-service/listOpenOnly?top=2');
            $obj = json_decode($json);
            $times = $obj->result;
            if ($times == []){
                echo "No Times"; 
            } else{
                foreach($times as $k) {
                    $open = $k->open;
                    echo "<li>Open: $close</li>";
                }
            }
            ?>
        </ul>

        <h1>Top 2 Close Times</h1>
        <ul>
            <?php           
            $json = file_get_contents('http://laptop-service/listCloseOnly?top=2');
            $obj = json_decode($json);
            $times = $obj->result;
            if ($times == []){
                echo "No Times"; 
            } else{
                foreach($times as $k) {
                    $close = $k->close;
                    echo "<li>Close: $close</li>";
                }
            }
            ?>
        </ul>


         <h1>All Times (csv)</h1>
        <ul>
        <?php
        $entry = file_get_contents('http://laptop-service/listAll/csv');
        echo nl2br($entry);           
            ?>
        </ul>


        <h1> Open Times Only (csv)</h1>
        <ul>
            <?php
            $data = file_get_contents('http://laptop-service/listOpenOnly/csv');
            echo nl2br($data);
            ?>
        </ul>


        <h1>Closed Times Only (csv)</h1>
        <ul>
            <?php
            $data = file_get_contents('http://laptop-service/listCloseOnly/csv');
            echo nl2br($data);
            ?>
        </ul>
    </body>
</html>
